<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Util;

use App\Config;
use Pheanstalk\Pheanstalk;

class Connection{
    
    private static $con = array();
    
    public static function getBeanstalkConnection($config = ''){
        
        if($config == ''){
            $config = 'beanstalk';
        }
        
        if(isSet(self::$con[$config])){
            return self::$con[$config];
        }
        
        $config_data  = Config::get($config);
        
        if($config_data && isSet($config_data['server'])){
            $server = $config_data['server'];
            
            try{
                
                $pheanstalk = new Pheanstalk($server);
                self::$con[$config] = $pheanstalk;
                
                return $pheanstalk;
            }  catch (Exception $e){
                throw new Exception( $e->getMessage());
            }
        }
    }
    
    public static function getMysqlConnection($config = ''){
        if($config == ''){
            $config = 'mysql';
        }
        
        if(isSet(self::$con[$config])){
            return self::$con[$config];
        }
        
        $config_data  = Config::get($config);
        
        if($config_data && isSet($config_data['server']) && isSet($config_data['user']) && isSet($config_data['password'])&& isSet($config_data['database'])){
            $server = $config_data['server'];
            $user = $config_data['user'];
            $password = $config_data['password'];
            $database = $config_data['database'];
            
            $mysql_con = mysqli_connect( $server , $user, $password, $database );
            
            self::$con[$config] = $mysql_con;
            
            return $mysql_con;
            
        }
    }
}
