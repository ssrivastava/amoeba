<?php

namespace App\Util;

use App\Util\Connection;


class MySql{
    
    private $config = '';
    
    function __construct($config = '') {
        $this->config = $config;
    }
    
    function getCon(){
        return Connection::getMysqlConnection();
    }

    
    function bindResults($stmt)
    {
        $meta = $stmt->result_metadata();
        $result = array();
        while ($field = $meta->fetch_field())
        {
            $result[$field->name] = NULL;
            $params[] = &$result[$field->name];
        }

        call_user_func_array(array($stmt, 'bind_result'), $params);

        return $result;
    }

    function getValues($row){
        $ret_arr = array();
        foreach ($row as $key=>$val){
            $ret_arr[$key] = $val;
        }
        return $ret_arr;
    }
    
    function query($query){
	
        $con = $this->getCon();
        
	$stmt = $con->prepare($query);
	$stmt->execute();
        
        $result = array();

        $row = $this->bindResults($stmt);

        if(!$stmt->error){
            while($stmt->fetch()){
                $result[] = $this->getValues ($row);
            }
        } else {
            $result = $stmt->error;
        }
        
        $stmt->close();
        return $result;
    }

    function queryGet($query, $param_string, $param_array, $return_type = 'row'){
	
        array_unshift($param_array, $param_string);

        $params = array();
        foreach($param_array as $key => $val){
            $params[$key] = &$param_array[$key];
        }

        $con = $this->getCon();

        $stmt = $con->prepare($query);
        call_user_func_array(array($stmt, "bind_param"), $params);
        $stmt->execute();

        $result = array();

        if($return_type == 'row'){

            $result = $this->bindResults($stmt);

            if(!$stmt->error){
                $stmt->fetch();
            } else {
                $result = $stmt->error;
            }
                
        }else{

            $row = $this->bindResults($stmt);

            if(!$stmt->error)
                while($stmt->fetch()){
                    $result[] = $this->getValues ($row);
                }
            else{
                $result = $stmt->error;
            }
                
        }
        
        $stmt->close();
        if(sizeof($result)){
            return $result;
        }
            
    }

    function queryInsert($query, $param_string, $param_array){
	
        array_unshift($param_array, $param_string);

        $params = array();
        foreach($param_array as $key => $val){
            $params[$key] = &$param_array[$key];
        }

        $con = $this->getCon();

        $stmt = $con->prepare($query);
        call_user_func_array(array($stmt, 'bind_param'), $params);
        $stmt->execute();

        if($stmt->error){
            $result = $stmt->error;
        } else{
            if($stmt->insert_id){
                $result = $stmt->insert_id;
            }
            else {
                $result = $stmt->affected_rows;
            }
        }

        $stmt->close();
        return $result;
    }

    function queryUpdate($query, $param_string, $param_array){
        
	array_unshift($param_array, $param_string);

        $params = array();
        foreach($param_array as $key => $val){
            $params[$key] = &$param_array[$key];
        }

        $con = $this->getCon();

        $stmt = $con->prepare($query);
        call_user_func_array(array($stmt, "bind_param"), $params);
        $stmt->execute();

        
        if(!$stmt->error){
            return true;
        } else {
            
            return false;
        }
    }

    function queryDelete($query, $param_string, $param_array){
        
	array_unshift($param_array, $param_string);

        $params = array();
        foreach($param_array as $key => $val){
            $params[$key] = &$param_array[$key];
        }

        $con = $this->getCon();
        
        $stmt = $con->prepare($query);
        call_user_func_array(array($stmt, "bind_param"), $params);
        $stmt->execute();

        if(!$stmt->error){
            $result = true;
        } else {
            $result = false;
        }
        
        $stmt->close();
        return $result;
    }
}
?>
