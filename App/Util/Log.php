<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Util;

class Log{
    
    const LEVEL_ERROR = 1;
    const LEVEL_WARNING = 2;
    const LEVEL_INFO = 3;
    
    private static function log($message, $level){
        
        $level_config = 3;
        
        if($level <= $level_config){
            //echo '<div class="" style="padding: 20px;">'.$message.'</div>';
            echo $message . '\n';
        }
    }
    
    public static function info($message){
        self::log($message, self::LEVEL_INFO);
    }
    
    public static function error($message){
        self::log($message, self::LEVEL_ERROR);
    }
    
    public static function warning($message){
        self::log($message, self::LEVEL_WARNING);
    }
}