<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;

use App\Util\Log;

class Processor{
    
    public function execute($data){
        
        $name = $data['name'];
        Log::info($name);
        Mapping::raiseMappedActions($name, $data, Config::JOB_TYPE_EVENT);
        
    }
    
}
