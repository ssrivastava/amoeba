<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;



class Config{
    
    const EVENT_TUBE = 'event_tube';
    const ACTION_TUBE = 'action_tube';
    
    const JOB_TYPE_EVENT = 1;
    const JOB_TYPE_ACTION = 2;
    
    
    
    private static $__config;
    
    public static function load(){
        
        if(empty(self::$__config)){
            
            self::$__config = parse_ini_file(CONF_FILE, true);
        
        }
    }
    
    public static function get($name){
        if(is_null(self::$__config)){
            self::load();
        }
        
        return isset(self::$__config[$name]) ? self::$__config[$name]  : null;
    }
    
    
    
}
