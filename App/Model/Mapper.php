<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use App\Util\MySql;
use App\Mapping;

class Mapper{
    
    public static $table = 'wh_mapping';
    public static $action_table = 'wh_actions';
    
    public static function map($params){
        
        $Mysql = new MySql();
        
        $table = self::$table;
        $query = "INSERT INTO $table ( event_id, mapped_id, mapping_type) VALUES( ?, ?, ?)";
        $param_string = 'iii';
        $param_array = array();
        $param_array[] = $params['event_id'];
        $param_array[] = $params['mapped_id'];
        $param_array[] = $params['type'];
        
        $Mysql->queryInsert($query, $param_string, $param_array);
        
    }
    
    public static function getMappings($id, $type){
        
        $table = self::$table;
        $action_table = self::$action_table;
        
        $query = "SELECT m.id as mapping_id,a.name,a.api as api_url, mapped_id FROM $table m JOIN $action_table a "
                . "WHERE m.mapped_id = a.id AND a.status = 1 AND event_id = ? AND mapping_type = ?";
        $param_string = 'ii';
        $param_array = array();
        $param_array[] = $id;
        $param_array[] = $type;
        
        $Mysql = new MySql();
        return $Mysql->queryGet($query, $param_string, $param_array, 'array');
        
        
    }
    
    public static function removeMapping($mapping_id){
        $table = self::$table;
        
        if(is_numeric($mapping_id)){
            $query = "DELETE FROM $table WHERE id = $mapping_id LIMIT 1";
            $Mysql = new MySql();
            $Mysql->query($query);
        }
        
        
        
    }
    
    
}