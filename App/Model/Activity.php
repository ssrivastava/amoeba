<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;

use App\Util\MySql;

class Activity{
    
    private $table = 'wh_activities';
    
    
    public function __construct() {
        
    }
    
    private function getMysql(){
        
    }
    
    public function get($id){
        
        $Mysql = new MySql();
        
        $query = "SELECT * FROM $this->table WHERE id = ?";
        $param_string = 's';
        $param_array = array($id);
        return $Mysql->queryGet($query, $param_string, $param_array);
        
    }
    
    public function getByName($name){
        
        $Mysql = new MySql();
        
        $query = "SELECT * FROM $this->table WHERE name = ? LIMIT 1";
        $param_string = 's';
        $param_array = array($name);
        
        $data = $Mysql->queryGet($query, $param_string, $param_array);
        
        return $data;
        
        
    }
    
    public function save($data){
        
        $Mysql = new MySql();
        
        if(isSet($data['id'])){
            $query = "UPDATE $this->table SET name = ?, entity_type = ?, description = ? WHERE id = ? ";
            $param_string = 'sssi';
            
            $param_array = array();
            $param_array[] = $data['name'];
            $param_array[] = $data['entity_type'];
            $param_array[] = $data['description'];
            $param_array[] = $data['id'];
            
            $bool = $Mysql->queryUpdate($query, $param_string, $param_array);
            if($bool){
                return $data['id'];
            }
            
        }else{
            $query = "INSERT INTO $this->table (name, entity_type, description, status) VALUES( ?, ?, ?, 1)";
            $param_string = 'sss';
            $param_array = array();
            $param_array[] = $data['name'];
            $param_array[] = $data['entity_type'];
            $param_array[] = $data['description'];
            
            return $Mysql->queryInsert($query, $param_string, $param_array);
        }
    }
    
    public function delete($id){
        
        $Mysql = new MySql();
        
        $query = "UPDATE $this->table SET status = 0 WHERE id = ?";
        $param_string = 'i';
        $param_array = array($id);
        
        $Mysql->queryUpdate($query, $param_string, $param_array);
        
    }
    
    public function getAll($params){
        
        $Mysql = new MySql();
        
        $letters = isSet($params['l']) ? $params['l'] : '';
        $start = isSet($params['start']) ? $params['start'] : 0;
        $limit = isset($params['limit']) ? $params['limit'] : 20;
        
        $data = array();
        
        if(strlen($letters)){
            $query = "SELECT * FROM $this->table WHERE name like ? and status = 1 LIMIT $start, $limit";
            $param_string = 's';
            $param_array = array("%{$letters}%");
            
            $data = $Mysql->queryGet($query, $param_string, $param_array,'array');
            return $data;
            
        }else{
            $query = "SELECT * FROM $this->table WHERE status = 1 LIMIT ?, ?";
            $param_string = 'ii';
            $param_array = array($start, $limit);
            
            $data = $Mysql->queryGet($query, $param_string, $param_array,'array');
        }
        
        return $data;
    }
    
}