<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;

use App\Api\ActionApi;
use App\Mapping;


class Actor{
    
    const ACT_TUBE = 'act_tube';


    function execute($data){
        
        $action_name = $data['name'];
        
        $api = $data['api_url'];
        $api_data = $data['api_data'];
        
        var_dump($api);
        
        $response = ActionApi::call($api, $api_data);
        //var_dump($response);
        if($response && is_array($response) && $response['ok'] == 1){
            $this->raiseMappedActions($action_name);
        }
        
    }
    
    function raiseMappedActions($action_name, $api_data){
        Mapping::raiseMappedActions($action_name, $api_data, Config::JOB_TYPE_ACTION);
    }
    
}
