<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;

use App\Util\Connection;
use \Exception;

class EventManager{
    
    public static function getPheanstalk(){
        
        $pheanstalk = Connection::getBeanstalkConnection();
        
        if(!$pheanstalk->getConnection()->isServiceListening()){
            sleep(1);
            $pheanstalk = $this->pheanstalk = \App\Studio\Connection\Connection::getPheanstalkCon();
            if(!$pheanstalk->getConnection()->isServiceListening()){

                $message = "Beanstalk Connection Failed";
                throw new \Exception($message);
            }
        }
        
        return $pheanstalk;
    }
    
    public static function listen($tube_name, $Executor){
    
        $pheanstalk = self::getPheanstalk();
        
        while(1){
            
            try{
                
                $job = $pheanstalk->watch($tube_name)->ignore('default')->reserve();
                $job_data = $job->getData();
                
                $Executor->execute(json_decode($job_data,true));
                $pheanstalk->delete($job);
                
            }catch(Exception $ex){
                
                $pheanstalk->bury($job);
                
            }
        }
    }
    
    
    public static function raiseEvent($tube_name, $event_data, $delay = 0){
        
        $pheanstalk = self::getPheanstalk();
        
        $priority = 100;        
        $json_data = json_encode($event_data);
            
        try{

            $pheanstalk->useTube($tube_name)->put($json_data, $priority, $delay);
            
        } catch(Exception $ex) {
            Except::trace($ex->getMessage());

            return false;
        }
        
        return true;
        
    }
    
}
