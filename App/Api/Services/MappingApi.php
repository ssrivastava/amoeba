<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Api\Services;

use App\Model\Mapper;
use App\Model\Activity;
use App\Model\Action;

class MappingApi{
    
    function saveActivity($params){
        
        $Activity = new Activity();
        $id = $Activity->save($params);
        
        return array(
            'status' => array('ok'    => 1, 'error' => ''),
            'response' => $id
            
        );
        
    }
    
    function deleteActivity($params){
        $Activity = new Activity();
        $Activity->delete($params['id']);
    }
    
    function saveAction($params){
        $Action = new Action();
        $id = $Action->save($params);
        
        return array(
            'status' => array('ok'    => 1, 'error' => ''),
            'response' => $id
            
        );
    }
    
    function deleteAction($params){
        $Action = new Action();
        $Action->delete($params['id']);
    }
    
    function getActivities($params){
        $Activity = new Activity();
        $data = $Activity->getAll($params);
        
        return array(
            'status' => array('ok'    => 1, 'error' => ''),
            'response' => $data
            
        );
    }
    
    function getActions($params){
        $Action = new Action();
        $data = $Action->getAll($params);
        
        return array(
            'status' => array('ok'    => 1, 'error' => ''),
            'response' => $data
            
        );
    }
    
    function map($params){
        Mapper::map($params);
    }
    
    function removeMapping($params){
        Mapper::removeMapping($params['mapping_id']);
    }
    
    function getMappings($params){
        $data = Mapper::getMappings($params['id'], $params['type']);
        return array(
            'status' => array('ok'    => 1, 'error' => ''),
            'response' => $data
            
        );
    }
    
}