<?php
/*
 * Author: Srijan
 * Date: 7th July 2014
 */
namespace App\Api;

class Api{
    
    public $call = '';
    public $params = array();
    public $response = null;
    public $uri_parts = array();
    
    const MAPPING_TYPE_CLASS = 'CLASS';
    const MAPPING_TYPE_FUNCTION = 'FUNCTION';
    
    
    public function __construct($call = 'null', $params = array()){
        if($call == 'null'){
            $params = $_REQUEST;
            $uri_parts = $this->getCall();
        }
        //Except::log(json_encode($uri_parts));
        $this->params = $params;
        $this->call = $_SERVER['REQUEST_URI'];
        
        $this->uri_parts = $uri_parts;
         
        if($this->auth()){
            $this->get();
            
            $this->render($this->response);
        }
    }
    
    private function getCall(){
        $call = $_SERVER['REQUEST_URI'];
        $parts = explode('?', $call);
        $request = $parts[0];
        
        //end point should be without http:// prefix
        $end_point = 'cro.com/JSY2/api/';
        
        $ignore_index = sizeof(explode('/', $end_point)) - 1;
        
        $request_parts = explode('/', $request);
        return array_slice($request_parts, $ignore_index);
        
    }
    
    private function getMapping($name, $type = ''){
        if($type == self::MAPPING_TYPE_CLASS){
            $name = 'Services\\'.ucfirst($name). 'Api';
        }
        return $name;
    }
    
    private function getClass(){
        $uri_parts = $this->uri_parts;
        if(isSet($uri_parts[0])){
            $class = $uri_parts[0];
            $class_name = $this->getMapping($class, self::MAPPING_TYPE_CLASS);
            $class_full_name = __NAMESPACE__ . "\\" . $class_name;
            if(class_exists($class_full_name)){
                return $class_full_name;
            }else{
		echo 'Class not found';
	    }
        }   
    }
    
    private function getFunction(){
        $uri_parts = $this->uri_parts;
        if(isSet($uri_parts[0])){
            $function = $uri_parts[1];
            return $this->getMapping($function, self::MAPPING_TYPE_FUNCTION);
        }
    }
    
    private function get(){
        $class_name = $this->getClass();
        if($class_name){
            $method_name = $this->getFunction();
            $Class = new $class_name();
            $params = $this->params;
            if(method_exists($Class, $method_name)){
                $response = $Class->{$method_name}($params);
            }else{
                $response = $class_name . " :: " . $method_name;
            }
            $this->response = $this->finalise($response, $method_name, $class_name);
            return $response;
        }
    }
    
    private function auth(){
        $request_ip = $_SERVER['REMOTE_ADDR'];
        if(strpos($request_ip, '192.169') === 0){
            return true;
        }else{
	    //echo 'Not Authorized';
		return true;
	}
    }
    
    public function render($response = false){
        $this->setContentType();
        if($response){
            echo json_encode($response);
        }
    }
    
    private function setContentType(){
        header('Content-Type: application/json');
    }
    
    private function finalise($response, $function_name, $class_name){
        if($response && is_array($response) && is_string($function_name)){
            //$response = Params::map($response, $function_name);
            // @ToDO: provide a mapper class
        }
        return $response;
    }
}

?>
