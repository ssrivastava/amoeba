<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;



class Producer{
    
    public static function raiseEvent($event_data){

        if(! self::isValidEvent($event_data)){
            
            return;
        
        }
        
        if($event_data['type'] == Config::JOB_TYPE_EVENT){
            $tube_name = Config::EVENT_TUBE;
        }else{
            $tube_name = Config::ACTION_TUBE;
        }
        
        
        EventManager::raiseEvent($tube_name, $event_data);
        
    }
    
    public static function isValidEvent($event_data){
        if(is_array($event_data)){
            return isSet($event_data['name']) && isSet($event_data['type']) && isSet($event_data['data']);
        }
        return false;
    }
    
    
    
}