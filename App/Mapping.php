<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use App\Model\Action;
use App\Model\Activity;

class Mapping{
    
    const MAPPING_ACTIVITY_ACTION = 1;
    const MAPPING_ACTION_ACTION = 2;
    
    public static function getMappedActions($name, $mapping_type){
        
        /*
         * mapping type :
         * 1 => activity to action 
         * 2 => action to action 
         */
        
        
        if($mapping_type == Mapping::MAPPING_ACTION_ACTION){
            $Action = new Action();
            $actionData = $Action->getByName($name);
            $id = $actionData['id'];
        }else{
            $Activity = new Activity();
            $activityData = $Activity->getByName($name);
            $id = $activityData['id'];
        }
        
        return Model\Mapper::getMappings($id, $mapping_type);
        
    }
    
    static function raiseMappedActions($name, $api_data, $type){
        
        if($type == Config::JOB_TYPE_EVENT){
            $mapped_actions = Mapping::getMappedActions($name, Mapping::MAPPING_ACTIVITY_ACTION);
        }else{
            $mapped_actions = Mapping::getMappedActions($name, Mapping::MAPPING_ACTION_ACTION);
        }
        
        //var_dump($mapped_actions);
        
        
        if($mapped_actions && is_array($mapped_actions)){
            
            foreach($mapped_actions as $action){
                
                $api_url = $action['api_url'];
                
                $event_data = array();
                $event_data['api_url'] = $api_url;
                $event_data['api_data'] = $api_data;
                $event_data['type'] = Config::JOB_TYPE_ACTION;
                
                var_dump($event_data);
                
                EventManager::raiseEvent(Config::ACTION_TUBE, $event_data);
                
            }
            
        }
    }
    
    
}