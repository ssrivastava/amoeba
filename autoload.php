<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('CORE_PATH', __DIR__ );
define('CONF_FILE', __DIR__ . '/config.ini.php');
    
    spl_autoload_register(function($className) {
        
        if(strpos($className, 'App\\') === 0){
            $libPath = CORE_PATH . '/';
        }else if(strpos($className, 'Pheanstalk') === 0){
            
            $className = preg_replace('/Pheanstalk/', '', $className, 1);
            $libPath = CORE_PATH . '/Lib/pheanstalk/src/';
            
        }
        
        
        $classFile = str_replace('\\',DIRECTORY_SEPARATOR,$className).'.php';
        $classPath = $libPath.$classFile;
        
        if (file_exists($classPath)) {
            require($classPath);
        }
    });