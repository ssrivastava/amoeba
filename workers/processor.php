<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include '../autoload.php';

use App\EventManager;
use App\Processor;
use App\Config;

$tube_name = Config::EVENT_TUBE;
$Executor = new Processor();

EventManager::listen($tube_name, $Executor);