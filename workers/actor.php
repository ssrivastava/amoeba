<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include '../autoload.php';

use App\EventManager;
use App\Actor;
use App\Config;

$tube_name = Config::ACTION_TUBE;
$Executor = new Actor();

EventManager::listen($tube_name, $Executor);